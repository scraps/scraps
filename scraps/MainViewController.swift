//
//  MainViewController.swift
//  scraps
//
//  Created by Matthew D'Alonzo on 2/25/18.
//  Copyright © 2018 Scraps. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import FirebaseCore
import FirebaseDatabase

//@interface FIRDatabaseReference : FIRDatabaseQuery



struct MapObject {
    var text = ""
    var latitude = 0.00
    var longitude = 0.00
}

var MapList = [GMSMarker()]
var MapCounter = 0

let camera = GMSCameraPosition.camera(withLatitude: 41.887,
                                      longitude:-87.622,
                                      zoom:15)
let mapView = GMSMapView.map(withFrame: .zero, camera:camera)


class MainViewController: UIViewController {
    
    var ref: DatabaseReference?

    

    
    override func loadView() {
        
        self.view = nil
        
        ref = Database.database().reference()


        
        //let key = ref.childByAutoId().key
        //ref
        //ref.child("noteText").setValue(["username": "Hello There"])
        //ref.child("noteText").setValue(["Gabeisaloser": "SecondThing"])
        
        //var notesRef = ref.observe(DataEventType.value, with: { (snapshot) in
            //let postDict = snapshot.value as? [String : String] ?? [:]
            // ...
        

        
            mapView.settings.scrollGestures = true
            mapView.settings.zoomGestures = true
            //let marker = GMSMarker()
            //marker.position = CLLocationCoordinate2D(latitude: 41.887, longitude: -87.622)
            //marker.appearAnimation = GMSMarkerAnimation.pop
            //marker.icon = UIImage(named: "NewMessage.png")
            //marker.map = mapView
            //marker.snippet = postDict["Note1"]
            
            self.view = mapView
            
        //})
        

        
        
    }


    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ref = Database.database().reference()
        
       ref?.child("Notes").observe(.childAdded, with: { (snapshot) in
            //Code to execute when child is added
            let marker = GMSMarker()
            MapList.append(marker)
            let value = snapshot.value as? NSDictionary
            print(snapshot)
            let finalLatitude = Double(value?["Latitude"] as? String ?? "0")
            let finalLongitude = Double(value?["Longitude"] as? String ?? "0")
            //print(value?["Latitude"] as? String)
            //print(value?["Longitude"] as? String)
            MapList[MapCounter].position = CLLocationCoordinate2D(latitude: finalLatitude!, longitude: finalLongitude!)
            MapList[MapCounter].appearAnimation = GMSMarkerAnimation.pop
            MapList[MapCounter].icon = UIImage(named: "NewMessage.png")
            MapList[MapCounter].map = mapView
            MapList[MapCounter].snippet = value?["Text"] as? String ?? "Nothing is here"
            //print(value?["Text"] as? String ?? "Nothing is here")
            //print(value?["Latitude"] as? String)
            //print(value?["Longitude"] as? String)
            MapCounter = MapCounter + 1
            //self.view = mapView


       })
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        self.view = nil
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
