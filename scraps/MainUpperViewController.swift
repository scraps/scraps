//
//  MainUpperViewController.swift
//  scraps
//
//  Created by Gabriel King on 08/04/18.
//  Copyright © 2018 Scraps. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import FirebaseCore
import FirebaseDatabase
import MapKit
import CoreLocation

class MainUpperViewController: UIViewController, CLLocationManagerDelegate {
    
    var ref: DatabaseReference?
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
   
    @IBOutlet weak var WriteText: UITextView!
    @IBOutlet weak var Latitude: UITextView!
    @IBOutlet weak var Longitude: UITextView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        ref = Database.database().reference()
        
        // Ask for Authorisation from the User.
        self.locManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locManager.delegate = self
            locManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locManager.startUpdatingLocation()
        }

        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func AddNote(_ sender: Any) {
        let key = ref?.child("Notes").childByAutoId()
        key?.setValue(["Text": WriteText.text, "Latitude": Latitude.text, "Longitude": Longitude.text])
        print("Latitude text " + Latitude.text)
        print("Longitude text " + Longitude.text)
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
