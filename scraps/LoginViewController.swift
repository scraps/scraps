//
//  LoginViewController.swift
//  scraps
//
//  Created by Gabriel King on 30/04/18.
//  Copyright © 2018 Scraps. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class LoginViewController: UIViewController {

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var signInSelector: UISegmentedControl!
    @IBOutlet weak var signInButton: UIButton!
   
    var isSignIn:Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signInSelectorChanged(_ sender: UISegmentedControl) {
        isSignIn = !isSignIn
        if isSignIn {
            signInLabel.text = "sign in"
            signInButton.setTitle("sign in", for: .normal)
            
        } else {
            signInLabel.text = "register"
            signInButton.setTitle("register", for: .normal)
        }
    }
    
    @IBAction func signInButtonTapped(_ sender: UIButton) {
        print("button tapped")
        if let email = emailTextField.text, let pass = passwordTextField.text {
            print (email, " ", pass)
            if isSignIn { // user attempting to sign in
                Auth.auth().signIn(withEmail: email, password: pass) { (user, error) in
                    print("user: ", user)
                    print("error: ", error)
                    if (error == nil) {
                        print("trying to segue")
                        self.performSegue(withIdentifier: "toMain", sender: self);
                    } else {
                        var autherr = String()
                        if let errCode = AuthErrorCode(rawValue: error!._code) {
                            switch errCode {
                            case .invalidEmail:
                                autherr = "invalid email address entered"
                            case .weakPassword:
                                autherr = "password too short"
                            case .userNotFound:
                                autherr = "user name or password incorrect"
                            case .wrongPassword:
                                autherr = "user name or password incorrect"
                            default:
                                autherr = "error with Firebase backend"
                            }
                            
                        }
                       let alert = UIAlertController(title: "Error", message: autherr, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: {(action) in
                            alert.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)

                        //[UIView alertWithTitle:bErrorTitle withError:error];
                        
                    }
                }
            } else { // user attempting to register
                Auth.auth().createUser(withEmail: email, password: pass) { (user, error) in
                   print(error)
                    if (error == nil) {
                        print("trying to segue")
                        self.performSegue(withIdentifier: "toMain", sender: self);
                    } else {
                        var autherr = String()
                        if let errCode = AuthErrorCode(rawValue: error!._code) {
                            switch errCode {
                            case .invalidEmail:
                                autherr = "invalid email address entered"
                            case .emailAlreadyInUse:
                                autherr = "email address already in use"
                            case .weakPassword:
                                autherr = "password too short"
                            default:
                                autherr = "error with Firebase backend"
                            }
                            
                        }
                        let alert = UIAlertController(title: "Error", message: autherr, preferredStyle: UIAlertControllerStyle.alert)
                        alert.addAction(UIAlertAction(title: "ok", style: UIAlertActionStyle.default, handler: {(action) in
                            alert.dismiss(animated: true, completion: nil)
                        }))
                        self.present(alert, animated: true, completion: nil)
                    }
                    }
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
