//
//  NearbyViewController.swift
//  scraps
//
//  Created by Gabriel King on 13/04/18.
//  Copyright © 2018 Scraps. All rights reserved.
//

import UIKit
import GoogleMaps
import Firebase
import FirebaseCore
import FirebaseDatabase
import MapKit
import CoreLocation

class NearbyViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var noteData = [String]()
    
    var ref: DatabaseReference?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.delegate = self
        tableView.dataSource = self
        
        ref = Database.database().reference()
        
        ref?.child("Notes").observe(.childAdded, with: { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            let individual_note = value?["Text"] as? String ?? "0"
            
            self.noteData.append(individual_note)
                
            self.tableView.reloadData()
            
        })
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        return noteData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "noteCell")
        cell?.textLabel?.text = noteData[indexPath.row]
        
        return cell!
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
