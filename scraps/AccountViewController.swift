//
//  AccountViewController.swift
//  scraps
//
//  Created by Gabriel King on 30/04/18.
//  Copyright © 2018 Scraps. All rights reserved.
//

import UIKit
import FirebaseAuth

class AccountViewController: UIViewController {
    
    
    
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var uid: UILabel!
    @IBOutlet weak var signout: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        email.text = "email: " + (Auth.auth().currentUser?.email)!
        uid.text = "uid: " + (Auth.auth().currentUser?.uid)!
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signouttapped(_ sender: Any) {
        do {
            try Auth.auth().signOut()
           // self.performSegue(withIdentifier: "toLogin", sender: self);
        } catch {
            
        }
        guard let appDel = UIApplication.shared.delegate as? AppDelegate else { return }
        let rootController = UIStoryboard(name: "Main", bundle: Bundle.main).instantiateViewController(withIdentifier: "login")
        appDel.window?.rootViewController = rootController
        let myCustomViewController: MainViewController = MainViewController(nibName: nil, bundle: nil)
        myCustomViewController.view = nil
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
